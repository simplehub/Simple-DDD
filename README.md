### DDD领域驱动设计



### 概念：
    从业务视角对软件系统进行拆解（有效的微服务拆解方案）
    业务和开发之间的矛盾：业务是现实世界的缩影，软件是虚拟世界的实现（计算机）。
    电子化，信息化，数字化都描述的是把物理世界迁移虚拟世界的过程.
    
### 引言
    传统贫血模型的问题
    1、接口的清晰度（可阅读性）
    2、数据验证和错误处理
    3、业务逻辑代码的清晰度
    4、可测试性。
    
    方案：Domain Primitive
    使用 Domain Primitive 的三原则
        让隐性的概念显性化
        让隐性的上下文显性化
        封装多对象行为
    常见的 DP 的使用场景包括：
        有格式限制的 String：比如Name，PhoneNumber，OrderNumber，ZipCode，Address等
        有限制的Integer：比如OrderId（>0），Percentage（0-100%），Quantity（>=0）等
        可枚举的 int ：比如 Status（一般不用Enum因为反序列化问题）
        Double 或 BigDecimal：一般用到的 Double 或 BigDecimal 都是有业务含义的，比如 Temperature、Money、Amount、ExchangeRate、Rating 等
        复杂的数据结构：比如 Map> 等，尽量能把 Map 的所有操作包装掉，仅暴露必要行为    
    
### DDD出现的契机
    降低物理世界翻译成虚拟世界的成本 
    解释：能不能有一种方式来减轻这个翻译过程呢。然后就发明了面向对象语言，开始尝试让计算机世界有物理世界的对象概念。
    面向对象还不够，这就有了DDD，DDD定义了一些基本概念，然后尝试让业务和开发都能够理解这些概念名词，
    然后让领域专家使用这些概念名词来描述业务，而由于使用了规定的概念名词，开发就可以很好的理解领域业务，
    并能够按照领域业务设计的方式进行软件实现。这就是DDD的初衷：让业务架构绑定系统架构。   

### 战略设计

    领域：问题域&解系统
      
    解系统：解系统可以映射一个限界上下文，是对问题域的一个特定，有效的解决方案
    
    限界上下文：一个由显示边界限定的特定职责。领域模型便存在于这个边界之内。在边界内，每一个模型概念，包括它的属性和操作，都具有特殊的含义
    
    划分限界上下文：从需求出发，按照领域的概念划分
        1、从产品所讲的通用语言中，提取专业术语为概念对象
        2、寻找概念对象直接的联系（提取动词，观察动词与对象直接的联系）
        3、将紧密耦合的划分在一起，观察各耦合之间的联系，形成对应的限界上下文
        
    限界上下文关系：
        合作关系（Partnership）：两个上下文紧密合作的关系，一荣俱荣，一损俱损。
        共享内核（Shared Kernel）：两个上下文依赖部分共享的模型。
        客户方-供应方开发（Customer-Supplier Development）：上下文之间有组织的上下游依赖。
        遵奉者（Conformist）：下游上下文只能盲目依赖上游上下文。
        防腐层（Anticorruption Layer）：一个上下文通过一些适配和转换与另一个上下文交互。
        开放主机服务（Open Host Service）：定义一种协议来让其他上下文来对本上下文进行访问。
        发布语言（Published Language）：通常与OHS一起使用，用于定义开放主机的协议。
        大泥球（Big Ball of Mud）：混杂在一起的上下文关系，边界不清晰。
        另谋他路（SeparateWay）：两个完全没有任何联系的上下文。
            
    领域模型（从订单的业务领域分析）
        核心领域：该领域业务最核心部分，其他领域业务依赖核心领域（订单）
        支撑领域：依赖核心领域的支撑领域（产品，商品）
        通用领域:可共用的通用支撑领域（user,permission）
### 战术设计

    聚合根: 聚合的根节点，一组相关对象的集合，作为整体被外访问
    聚合  
    实体：对象区分有唯一标识(product)
    值对象：对象区分没有唯一标识（订单adress）
    
    资源库 （持久化聚合实体数据）
    应用服务:跨领域服务逻辑组装
    适配服务：Driving Adapter  &  Driven Adapter
    领域服务（当前领域上线文中使用）
    门面服务（防腐层）：不同领域上下文之间访问
    
    领域事件： 服务读写分离，通过领域事件进行数据传递，保证数据的最终一致性
    CQRS ：command query responsibility segregation
    
    六边形架构
    
### 代码实现
    写操作： app - port - application - AR（aggregate) - respository - store 
    读操作：
    1、基于领域模型：获取聚合根，将聚合根转化为需要展示的接口数据
        缺点：
            束缚于边界上下文，例如获取order与product信息，需要将order边界聚合根和product边界聚合根加载内存进行数据组装。
            跨边界数据获取组装困难（数据中台）
    2、基于数据模型：查询基于数据模型，通过sql或者service的方式获取数据，不再需要受限于领域模型的设计
        缺点：
            读操作还是受限于写模型 
    3、基于CQRS模式：Command and Query responsibility segregation 
        优点：完全解耦写模型与读模型，读写模型之间的数据同步通过Domain Event来保证最终一致性。
        缺点：实现相对复杂
                         


#### 介绍
    {**以下是码云平台说明，您可以替换此简介**
    码云是 OSCHINA 推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
    无论是个人、团队、或是企业，都能够用码云实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

#### 软件架构
    软件架构说明


#### 安装教程

    1. xxxx
    2. xxxx
    3. xxxx

#### 使用说明

    1. xxxx
    2. xxxx
    3. xxxx

#### 参与贡献
    
    1. Fork 本仓库
    2. 新建 Feat_xxx 分支
    3. 提交代码
    4. 新建 Pull Request


#### 码云特技
    1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
    2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
    3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
    4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
    5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
    6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)