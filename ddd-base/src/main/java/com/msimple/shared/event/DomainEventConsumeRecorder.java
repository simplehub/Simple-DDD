package com.msimple.shared.event;

public interface DomainEventConsumeRecorder {

    boolean record(DomainEvent event);

    void deleteAll();
}
