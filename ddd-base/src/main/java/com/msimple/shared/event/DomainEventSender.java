package com.msimple.shared.event;

public interface DomainEventSender {
    void send(DomainEvent event);
}
