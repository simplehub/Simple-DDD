package com.msimple.shared.exception;

/**
 *  common error code
 * @Title:      CommonErrorCode
 * @Package:    com.msimple.shared.exception.CommonErrorCode
 * @Author:     M.simple
 * @Remark:     The modified content
 * @CreateDate: 2019-10-13 15:40
 * @Version:    v2.0
 */
public enum CommonErrorCode implements ErrorCode {
    
    LOCK_OCCUPIED(409, "任务已被锁定，请稍后重试"),
    SYSTEM_ERROR(500, "系统错误");
    private int status;
    private String message;

    CommonErrorCode(int status, String message) {
        this.status = status;
        this.message = message;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getCode() {
        return this.name();
    }
}
