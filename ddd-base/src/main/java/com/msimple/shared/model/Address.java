package com.msimple.shared.model;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.util.Assert;

import java.util.Objects;

/**
 * order address
 * @Title:      Address
 * @Package:    com.msimple.shared.model.Address
 * @Author:     M.simple
 * @Remark:     The modified content
 * @CreateDate: 2019-10-13 16:20
 * @Version:    v2.0
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Address {
    
    private String province;
    private String city;
    private String detail;

    private Address(String province, String city, String detail) {

        Assert.notNull(province, "省份不能为空");
        Assert.notNull(city, "城市不能为空");


        this.province = province;
        this.city = city;
        this.detail = detail;
    }

    public static Address of(String province, String city, String detail) {
        return new Address(province, city, detail);
    }

    public Address changeDetailTo(String detail) {
        return new Address(this.province, this.city, detail);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Address address = (Address) o;
        return province.equals(address.province) &&
                city.equals(address.city) &&
                detail.equals(address.detail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(province, city, detail);
    }

    public String getProvince() {
        return province;
    }

    public String getCity() {
        return city;
    }

    public String getDetail() {
        return detail;
    }
}
