package com.msimple.shared.utils;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

public class ListCommand<T> extends ArrayList<T> {
    @Valid
    public List<T> getList() {
        return this;
    }
}
