package com.msimple.spring.common.event.messaging.kafka;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

@Data
@ConfigurationProperties("msimple.kafka")
@Validated
public class EcommerceKafkaProperties {


}
