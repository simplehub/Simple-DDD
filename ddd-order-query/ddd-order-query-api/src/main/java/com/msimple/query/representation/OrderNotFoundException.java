package com.msimple.query.representation;

import com.msimple.shared.exception.AppException;

import static com.google.common.collect.ImmutableMap.of;

public class OrderNotFoundException extends AppException {
    public OrderNotFoundException(String orderId) {
        super(OrderQueryErrorCode.ORDER_NOT_FOUND, of("orderId", orderId));
    }
}
