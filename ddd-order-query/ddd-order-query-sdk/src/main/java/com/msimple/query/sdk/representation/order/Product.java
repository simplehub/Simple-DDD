package com.msimple.query.sdk.representation.order;

import lombok.Value;

@Value
public class Product {
    private String id;
    private String name;
    private String description;
}
