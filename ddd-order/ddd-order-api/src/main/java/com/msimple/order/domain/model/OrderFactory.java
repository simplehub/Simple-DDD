package com.msimple.order.domain.model;

import com.msimple.shared.model.Address;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 *  order factory because logic need order domain service
 * @Title:      OrderFactory
 * @Package:    OrderFactory
 * @Author:     M.simple
 * @Remark:     The modified content
 * @CreateDate: 2019-10-13 16:09
 * @Version:    v2.0
 */
@Component
public class OrderFactory {
    
    private final OrderIdGenerator idGenerator;

    public OrderFactory(OrderIdGenerator idGenerator) {
        this.idGenerator = idGenerator;
    }

    /**
     * create order info
     * 1.generate order number
     * 2.create order info
     * 3.create sub order
     * 4.create room info
     * 5.create product info snapshot
     * @method      create
     * @param       items:address
     * @return      Order
     * @author      M.simple
     * @date        2019-10-12 11:25
     * @version     v2.0
     */
    public Order create(List<OrderProductItem> items, Address address) {

        String orderId = idGenerator.generate();
        return Order.create(orderId, items, address);
    }
}
