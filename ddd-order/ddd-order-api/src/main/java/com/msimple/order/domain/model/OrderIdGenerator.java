package com.msimple.order.domain.model;

import com.msimple.shared.utils.UuidGenerator;
import org.springframework.stereotype.Component;

/**
 *  OrderIdGenerator
 * @Title:      OrderIdGenerator
 * @Package:    OrderIdGenerator
 * @Author:     M.simple
 * @Remark:     The modified content
 * @CreateDate: 2019-10-13 16:22
 * @Version:    v2.0
 */
@Component
public class OrderIdGenerator {

    public String generate() {
        return UuidGenerator.newUuid();
    }
}
