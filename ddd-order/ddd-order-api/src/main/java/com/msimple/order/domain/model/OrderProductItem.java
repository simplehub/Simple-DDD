package com.msimple.order.domain.model;

import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;

/**
 *  order product item
 * @Title:      OrderProductItem
 * @Package:    OrderProductItem
 * @Author:     M.simple
 * @Remark:     The modified content
 * @CreateDate: 2019-10-13 16:20
 * @Version:    v2.0
 */
@Getter
@Builder
public class OrderProductItem {
    
    private String productId;

    private int count;
    
    private BigDecimal itemPrice;

    public static OrderProductItem create(String productId, int count, BigDecimal itemPrice) {
        return OrderProductItem.builder()
                .productId(productId)
                .count(count)
                .itemPrice(itemPrice)
                .build();
    }

    BigDecimal totalPrice() {
        return itemPrice.multiply(BigDecimal.valueOf(count));
    }

    public void updateCount(int count) {
        this.count = count;
    }

    public String getProductId() {
        return productId;
    }

    public int getCount() {
        return count;
    }

    public BigDecimal getItemPrice() {
        return itemPrice;
    }
}
