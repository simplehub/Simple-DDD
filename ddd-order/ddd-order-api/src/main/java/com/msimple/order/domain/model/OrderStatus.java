package com.msimple.order.domain.model;

/**
 *  OrderStatus
 * @Title:      OrderStatus
 * @Package:    OrderStatus
 * @Author:     M.simple
 * @Remark:     The modified content
 * @CreateDate: 2019-10-13 16:22
 * @Version:    v2.0
 */
public enum OrderStatus {
    
    CREATED,
    
    PAID
}
