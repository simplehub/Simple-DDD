package com.msimple.order.facade;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 *  OrderPaymentProxy
 * @Title:      OrderPaymentProxy
 * @Package:    OrderPaymentProxy
 * @Author:     M.simple
 * @Remark:     The modified content
 * @CreateDate: 2019-10-13 16:24
 * @Version:    v2.0
 */
@Component
public class OrderPaymentProxy {
    
    public void pay(String orderId, BigDecimal paidPrice) {
        //call remote payment service
    }
}
