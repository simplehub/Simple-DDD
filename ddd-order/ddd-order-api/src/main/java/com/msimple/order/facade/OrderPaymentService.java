package com.msimple.order.facade;

import com.msimple.order.domain.model.Order;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 *  OrderPaymentService
 * @Title:      OrderPaymentService
 * @Package:    OrderPaymentService
 * @Author:     M.simple
 * @Remark:     The modified content
 * @CreateDate: 2019-10-13 16:24
 * @Version:    v2.0
 */
@Component
public class OrderPaymentService {
    
    private final OrderPaymentProxy paymentProxy;

    public OrderPaymentService(OrderPaymentProxy paymentProxy) {
        this.paymentProxy = paymentProxy;
    }

    public void pay(Order order, BigDecimal paidPrice) {
        order.pay(paidPrice);
        paymentProxy.pay(order.getId(), paidPrice);
    }
}
