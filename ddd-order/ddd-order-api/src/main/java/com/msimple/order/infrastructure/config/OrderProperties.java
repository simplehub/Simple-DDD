package com.msimple.order.infrastructure.config;


import org.hibernate.validator.constraints.Range;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

/**
 *  OrderProperties
 * @Title:      OrderProperties
 * @Package:    OrderProperties
 * @Author:     M.simple
 * @Remark:     The modified content
 * @CreateDate: 2019-10-13 16:24
 * @Version:    v2.0
 */
@Component
@ConfigurationProperties("msimple.order")
@Validated
public class OrderProperties {
    @NotBlank
    private String jwtSecret;

    @Range(min = 1)
    private long jwtExpireMinutes;

    public String getJwtSecret() {
        return jwtSecret;
    }

    public void setJwtSecret(String jwtSecret) {
        this.jwtSecret = jwtSecret;
    }

    public long getJwtExpireMinutes() {
        return jwtExpireMinutes;
    }

    public void setJwtExpireMinutes(long jwtExpireMinutes) {
        this.jwtExpireMinutes = jwtExpireMinutes;
    }
}
