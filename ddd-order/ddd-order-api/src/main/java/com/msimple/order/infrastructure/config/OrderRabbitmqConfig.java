package com.msimple.order.infrastructure.config;

import com.msimple.spring.common.event.messaging.rabbit.EcommerceRabbitProperties;
import org.springframework.amqp.core.Binding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.springframework.amqp.core.Binding.DestinationType.QUEUE;

/**
 *  OrderRabbitmqConfig
 * @Title:      OrderRabbitmqConfig
 * @Package:    OrderRabbitmqConfig
 * @Author:     M.simple
 * @Remark:     The modified content
 * @CreateDate: 2019-10-13 16:24
 * @Version:    v2.0
 */
@Configuration
public class OrderRabbitmqConfig {


    private EcommerceRabbitProperties properties;

    public OrderRabbitmqConfig(EcommerceRabbitProperties properties) {
        this.properties = properties;
    }


    //将order上下文的"接收方queue"绑定到自身的"发送方exchange"，用于CQRS异步更新OrderSummary
    @Bean
    public Binding bindToInventoryChanged() {
        return new Binding(properties.getReceiveQ(), QUEUE, "order-publish-x", "com.msimple.order.sdk.event.order.#", null);
    }


}
