package com.msimple.order.infrastructure.exception;

import com.msimple.shared.exception.AppException;

import static com.msimple.order.infrastructure.exception.error.OrderErrorCode.ORDER_CANNOT_BE_MODIFIED;
import static com.google.common.collect.ImmutableMap.of;

/**
 *  java类简单作用描述
 * @Title:      OrderCannotBeModifiedException
 * @Package:    com.msimple.order.infrastructure.exception.OrderCannotBeModifiedException
 * @Author:     M.simple
 * @Remark:     The modified content
 * @CreateDate: 2019-10-15 15:30
 * @Version:    v2.0
 */
public class OrderCannotBeModifiedException extends AppException {
    public OrderCannotBeModifiedException(String id) {
        super(ORDER_CANNOT_BE_MODIFIED, of("orderId", id));
    }
}
