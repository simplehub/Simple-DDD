package com.msimple.order.infrastructure.exception;

import com.msimple.shared.exception.AppException;

import static com.google.common.collect.ImmutableMap.of;
import static com.msimple.order.infrastructure.exception.error.OrderErrorCode.ORDER_NOT_FOUND;

/**
 *  OrderNotFoundException
 * @Title:      OrderNotFoundException
 * @Package:    com.msimple.order.infrastructure.exception.OrderNotFoundException
 * @Author:     M.simple
 * @Remark:     The modified content
 * @CreateDate: 2019-10-13 17:40
 * @Version:    v2.0
 */
public class OrderNotFoundException extends AppException {
    
    public OrderNotFoundException(String orderId) {
        super(ORDER_NOT_FOUND, of("orderId", orderId));
    }
}
