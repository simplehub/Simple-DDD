package com.msimple.order.infrastructure.exception;

import com.msimple.shared.exception.AppException;

import static com.msimple.order.infrastructure.exception.error.OrderErrorCode.PAID_PRICE_NOT_SAME_WITH_ORDER_PRICE;
import static com.google.common.collect.ImmutableMap.of;

/**
 *  java类简单作用描述
 * @Title:      PaidPriceNotSameWithOrderPriceException
 * @Package:    com.msimple.order.infrastructure.exception.PaidPriceNotSameWithOrderPriceException
 * @Author:     M.simple
 * @Remark:     The modified content
 * @CreateDate: 2019-10-15 15:30
 * @Version:    v2.0
 */
public class PaidPriceNotSameWithOrderPriceException extends AppException {
    public PaidPriceNotSameWithOrderPriceException(String id) {
        super(PAID_PRICE_NOT_SAME_WITH_ORDER_PRICE, of("orderId", id));
    }
}
