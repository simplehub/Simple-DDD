package com.msimple.order.infrastructure.exception;

import com.msimple.shared.exception.AppException;

import static com.msimple.order.infrastructure.exception.error.OrderErrorCode.PRODUCT_NOT_IN_ORDER;
import static com.google.common.collect.ImmutableMap.of;

/**
 *  java类简单作用描述
 * @Title:      ProductNotInOrderException
 * @Package:    com.msimple.order.infrastructure.exception.ProductNotInOrderException
 * @Author:     M.simple
 * @Remark:     The modified content
 * @CreateDate: 2019-10-15 15:30
 * @Version:    v2.0
 */
public class ProductNotInOrderException extends AppException {
    public ProductNotInOrderException(String productId, String orderId) {
        super(PRODUCT_NOT_IN_ORDER, of("productId", productId,
                "orderId", orderId));
    }
}
