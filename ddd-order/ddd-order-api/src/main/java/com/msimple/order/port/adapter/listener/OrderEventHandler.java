package com.msimple.order.port.adapter.listener;

import com.msimple.order.application.representation.OrderRepresentationService;
import com.msimple.sdk.event.order.OrderEvent;
import org.springframework.stereotype.Component;

/**
 *  OrderEventHandler
 * @Title:      OrderEventHandler
 * @Package:    OrderEventHandler
 * @Author:     M.simple
 * @Remark:     The modified content
 * @CreateDate: 2019-10-13 16:23
 * @Version:    v2.0
 */
@Component
public class OrderEventHandler {
    private final OrderRepresentationService orderRepresentationService;

    public OrderEventHandler(OrderRepresentationService orderRepresentationService) {
        this.orderRepresentationService = orderRepresentationService;
    }

    public void cqrsSync(OrderEvent event) {
        orderRepresentationService.cqrsSync(event.getOrderId());
    }
}
