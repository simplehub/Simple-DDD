package com.msimple.order.port.adapter.listener;

import com.msimple.order.order.OrderEventHandler;
import com.msimple.sdk.event.order.OrderEvent;
import com.msimple.spring.common.event.messaging.rabbit.EcommerceRabbitListener;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.stereotype.Component;

/**
 *  OrderRabbitListener
 * @Title:      OrderRabbitListener
 * @Package:    OrderRabbitListener
 * @Author:     M.simple
 * @Remark:     The modified content
 * @CreateDate: 2019-10-13 16:24
 * @Version:    v2.0
 */
@Component
@EcommerceRabbitListener
public class OrderRabbitListener {
    private OrderEventHandler eventHandler;

    public OrderRabbitListener(OrderEventHandler eventHandler) {
        this.eventHandler = eventHandler;
    }

    @RabbitHandler
    public void on(OrderEvent event) {
        eventHandler.cqrsSync(event);
    }

}
