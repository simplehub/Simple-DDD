package com.msimple.sdk.command.order;

import lombok.Value;

import javax.validation.constraints.NotNull;

/**
 *  ChangeAddressDetailCommand
 * @Title:      ChangeAddressDetailCommand
 * @Package:    ChangeAddressDetailCommand
 * @Author:     M.simple
 * @Remark:     The modified content
 * @CreateDate: 2019-10-13 16:26
 * @Version:    v2.0
 */
@Value
public class ChangeAddressDetailCommand {
    
    @NotNull(message = "详细地址不能为空")
    private String detail;

}
