package com.msimple.sdk.command.order;

import lombok.Value;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

/**
 *  ChangeProductCountCommand
 * @Title:      ChangeProductCountCommand
 * @Package:    ChangeProductCountCommand
 * @Author:     M.simple
 * @Remark:     The modified content
 * @CreateDate: 2019-10-13 16:26
 * @Version:    v2.0
 */
@Value
public class ChangeProductCountCommand {
    
    @NotBlank(message = "产品ID不能为空")
    private String productId;

    @Min(value = 1, message = "产品数量必须大于0")
    private int count;

}
