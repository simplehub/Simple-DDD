package com.msimple.sdk.command.order;

import com.msimple.shared.model.Address;
import lombok.Value;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 *  java类简单作用描述
 * @Title:      CreateOrderCommand
 * @Package:    CreateOrderCommand
 * @Author:     M.simple
 * @Remark:     The modified content
 * @CreateDate: 2019-10-13 16:28
 * @Version:    v2.0
 */
@Value
public class CreateOrderCommand {
    @Valid
    @NotEmpty(message = "订单项不能为空")
    private List<OrderItemCommand> items;

    @NotNull(message = "订单地址不能为空")
    private Address address;

}
