package com.msimple.sdk.command.order;

import lombok.Value;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 *  PayOrderCommand
 * @Title:      PayOrderCommand
 * @Package:    PayOrderCommand
 * @Author:     M.simple
 * @Remark:     The modified content
 * @CreateDate: 2019-10-13 16:27
 * @Version:    v2.0
 */
@Value
public class PayOrderCommand {

    @NotNull(message = "支付金额不能为空")
    private BigDecimal paidPrice;

}
