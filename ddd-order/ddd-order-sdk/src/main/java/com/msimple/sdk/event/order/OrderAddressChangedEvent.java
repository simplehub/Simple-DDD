package com.msimple.sdk.event.order;

import lombok.Getter;

import java.beans.ConstructorProperties;

/**
 *  OrderAddressChangedEvent
 * @Title:      OrderAddressChangedEvent
 * @Package:    OrderAddressChangedEvent
 * @Author:     M.simple
 * @Remark:     The modified content
 * @CreateDate: 2019-10-13 16:27
 * @Version:    v2.0
 */
@Getter
public class OrderAddressChangedEvent extends OrderEvent {
    private String oldAddress;
    private String newAddress;

    @ConstructorProperties({"orderId", "oldAddress", "newAddress"})
    public OrderAddressChangedEvent(String orderId, String oldAddress, String newAddress) {
        super(orderId);
        this.oldAddress = oldAddress;
        this.newAddress = newAddress;
    }
}
