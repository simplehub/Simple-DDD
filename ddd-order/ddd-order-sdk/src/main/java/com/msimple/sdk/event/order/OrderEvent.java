package com.msimple.sdk.event.order;

import com.msimple.shared.event.DomainEvent;
import lombok.Getter;

/**
 *  OrderEvent
 * @Title:      OrderEvent
 * @Package:    OrderEvent
 * @Author:     M.simple
 * @Remark:     The modified content
 * @CreateDate: 2019-10-13 16:27
 * @Version:    v2.0
 */
@Getter
public abstract class OrderEvent extends DomainEvent {
    
    private String orderId;

    protected OrderEvent(String orderId) {
        this.orderId = orderId;
    }
}
