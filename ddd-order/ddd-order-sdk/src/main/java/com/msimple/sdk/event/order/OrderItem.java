package com.msimple.sdk.event.order;

import lombok.Value;

/**
 *  OrderItem
 * @Title:      OrderItem
 * @Package:    OrderItem
 * @Author:     M.simple
 * @Remark:     The modified content
 * @CreateDate: 2019-10-13 16:28
 * @Version:    v2.0
 */
@Value
public class OrderItem {
    
    private String productId;
    
    private int count;
}
