package com.msimple.sdk.event.order;

import lombok.Getter;

import java.beans.ConstructorProperties;

 /**
  *  OrderPaidEvent
  * @Title:      OrderPaidEvent
  * @Package:    OrderPaidEvent
  * @Author:     M.simple
  * @Remark:     The modified content
  * @CreateDate: 2019-10-13 16:28
  * @Version:    v2.0
  */
@Getter
public class OrderPaidEvent extends OrderEvent {

    @ConstructorProperties({"orderId"})
    public OrderPaidEvent(String orderId) {
        super(orderId);
    }
}
