package com.msimple.sdk.event.order;

import lombok.Getter;

import java.beans.ConstructorProperties;

/**
 *  OrderProductChangedEvent
 * @Title:      OrderProductChangedEvent
 * @Package:    OrderProductChangedEvent
 * @Author:     M.simple
 * @Remark:     The modified content
 * @CreateDate: 2019-10-13 16:28
 * @Version:    v2.0
 */
@Getter
public class OrderProductChangedEvent extends OrderEvent {
    private String productId;
    private int originCount;
    private int newCount;

    @ConstructorProperties({"orderId", "productId", "originCount", "newCount"})
    public OrderProductChangedEvent(String orderId, String productId, int originCount, int newCount) {
        super(orderId);
        this.productId = productId;
        this.originCount = originCount;
        this.newCount = newCount;
    }
}
