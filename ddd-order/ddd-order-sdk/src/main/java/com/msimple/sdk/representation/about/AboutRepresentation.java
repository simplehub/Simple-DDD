package com.msimple.sdk.representation.about;


import lombok.Value;

/**
 *  AboutRepresentation
 * @Title:      AboutRepresentation
 * @Package:    AboutRepresentation
 * @Author:     M.simple
 * @Remark:     The modified content
 * @CreateDate: 2019-10-13 16:28
 * @Version:    v2.0
 */
@Value
public class AboutRepresentation {
    private String appName;
    private String buildNumber;
    private String buildTime;
    private String deployTime;
    private String gitRevision;
    private String gitBranch;
    private String environment;

}
