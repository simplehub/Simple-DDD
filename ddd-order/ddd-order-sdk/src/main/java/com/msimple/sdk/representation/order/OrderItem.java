package com.msimple.sdk.representation.order;

import lombok.Value;

import java.math.BigDecimal;

/**
 *  OrderItem
 * @Title:      OrderItem
 * @Package:    OrderItem
 * @Author:     M.simple
 * @Remark:     The modified content
 * @CreateDate: 2019-10-13 16:28
 * @Version:    v2.0
 */
@Value
public class OrderItem {
    private final String productId;
    private final int count;
    private final BigDecimal itemPrice;
}
