package com.msimple.sdk.representation.order;


import com.msimple.shared.model.Address;
import lombok.Value;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

/**
 *  OrderRepresentation
 * @Title:      OrderRepresentation
 * @Package:    OrderRepresentation
 * @Author:     M.simple
 * @Remark:     The modified content
 * @CreateDate: 2019-10-13 16:28
 * @Version:    v2.0
 */
@Value
public class OrderRepresentation {
    private String id;
    private List<OrderItem> items;
    private BigDecimal totalPrice;
    private String status;
    private Address address;
    private Instant createdAt;

}
