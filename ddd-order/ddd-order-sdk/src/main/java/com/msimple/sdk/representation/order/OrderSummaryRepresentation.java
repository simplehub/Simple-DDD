package com.msimple.sdk.representation.order;

import com.msimple.shared.model.Address;
import lombok.Value;

import java.math.BigDecimal;
import java.time.Instant;

/**
 *  OrderRepresentation
 * @Title:      OrderSummaryRepresentation
 * @Package:    OrderSummaryRepresentation
 * @Author:     M.simple
 * @Remark:     The modified content
 * @CreateDate: 2019-10-13 16:29
 * @Version:    v2.0
 */
@Value
public class OrderSummaryRepresentation {
    
    private String id;
    private BigDecimal totalPrice;
    private String status;
    private Instant createdAt;
    private Address address;
}
