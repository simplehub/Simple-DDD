package com.msimple.product.sdk.event.category;

import com.msimple.shared.event.DomainEvent;
import lombok.Getter;

@Getter
public abstract class CategoryEvent extends DomainEvent {
    private String categoryId;

    protected CategoryEvent(String categoryId) {
        this.categoryId = categoryId;
    }

}
