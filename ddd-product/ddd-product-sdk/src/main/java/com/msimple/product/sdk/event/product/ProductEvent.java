package com.msimple.product.sdk.event.product;

import com.msimple.shared.event.DomainEvent;
import lombok.Getter;

@Getter
public abstract class ProductEvent extends DomainEvent {
    private String productId;

    protected ProductEvent(String productId) {
        this.productId = productId;
    }

}
